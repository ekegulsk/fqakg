## FQAKG

FQAKG (Factoid Question Answering using Knowledge Graph) - experimental Question Answering system utilizing Knowledge Graphs and Deep Learning for finding exact short answers to factoid questions.

## Code Example
This code example assumes that you have opened the project in a python IDE (e.g Pycharm) and create a new file to run 
the following code.
    
    from KGQA import *
    from instances import *
    question = "How tall is Mount McKinley?"
    question_id = 1
    diffbot_instance = Intances.Diffbot()
    google_instance = Instances.Google()
    dkg_key = "xxxxxxxxx" # put your diffbot Knowledge Graph API Key
    gkg_key = "xxxxxxxxx" # put your google knowledge graph API Key
    dkgqa = KGQA(question, question_id, verbose=True, kg_instance=diffbot_instance, api_key=key) # using DKG
    gkgqa = KGQA(question, question_id, verbose=True, kg_instance=google_instance, api_key=key) # using GKG
    dkgqa_answer = dkgqa.answer()
    gkgqa_answer = gkgqa.answer()

## Motivation

This project was part of the requeriments for my MS of Science in Computer Science, SFSU, Summer 2018

## Installation

1. Install pip3 (python 3.x)
2. Clone or download kgqa project
3. Install project requirements (file is in ~/kgqa)

       pip install -r requirements.txt
       
       
4. Open the project in your prefered IDE
5. Create a file and run the example code
6. For running this project from terminal, you'll need to create a __Main__ file to point out the
   reference starting point file.

## API Reference

FQAKG uses custom APIs included in the project in order to extract data from the knowledge graph instances supported

## Tests

The **test** folder contains methods to perform evaluation experiments using the system. Datasets are zipped into a file
located in the "datasets sub-folder. This file needs to be unzipped before using the
the system. The following tests exist:

### Live QA 2015
This test uses questions from Live QA 2015 (https://trec.nist.gov/data/qa/2015_LiveQA.html) and compares the answers by KGQA2 system to the human answers. It uses cosine
similarity and Jaccard similarity for assessing how the two differ.
To run, execute the command from the terminal:
>python LiveQA_2015.py -kg google // this will use Google KG

>python LiveQA_2015.py -kg diffbot // this will use Diffbot KG


### Live QA 2016
This test uses questions from Live QA 2016 (https://trec.nist.gov/data/qa/2016_LiveQA.html) and compares the answers by KGQA2 system to the human answers. It uses cosine
similarity and Jaccard similarity for assessing how the two differ.
To run, execute the command from the terminal:
>python LiveQA_2016.py -kg google // this will use Google KG

>python LiveQA_2016.py -kg diffbot // this will use Diffbot KG

## Contributors

Jose Ortiz Costa, MS in Computer Science. jortizco@mail.sfsu.edu

Professor Anagha Kulkarni, from San Francisco State University. ak@sfsu.edu

Eduard Kegulskiy, MS in Computer Science, ed_dvd@yahoo.com