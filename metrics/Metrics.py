from rank_metrics import *
class Metrics(object):
    def __init__(self, relevances):
        """
        
        :param relevances: list of relevance scores
        """
        self._relevances = relevances
        self._rel_len = len(relevances)

    def avg_score(self):
        sum_rel = 0
        total_num_rel = len(self._relevances)
        for rel in self._relevances:
            rel = rel-1
            sum_rel += rel
        print(sum_rel, total_num_rel)
        return sum_rel/(total_num_rel)

    def success_at(self, success_value):
        sum_succ = 0
        total_num_rel = self._rel_len
        index = 1
        for rel in self._relevances:
            if index > total_num_rel:
                break
            if rel == 0:
                rel = 1
            if rel >= success_value:
                sum_succ += 1
            index+=1
        return sum_succ / (total_num_rel)

    def precission_at(self, precision_value):
        sum_succ = 0
        total_num_rel = 0
        for rel in self._relevances:
            if rel == 0:
                rel = 1
            if rel > 0:
                total_num_rel+=1
            else:
                total_num_rel-=1
            if rel >= precision_value:
                sum_succ += 1
        return sum_succ / (total_num_rel)

    def ndcg_at(self):
        return ndcg_at_k(self._relevances, self._rel_len)

    def questions_with_no_results(self):
        sum_no_r = 0
        total_num_rel = len(self._relevances)
        for rel in self._relevances:
            if rel == 0:
                sum_no_r += 1
        return sum_no_r




# KGKA
#metrics = Metrics([4,4,2,4,4,3,4,2,4,2,4,4,1,4,4,2,4,1,4,4,4,4,1,3,2,3,4,3,4,4,4,4,4,4,4,1,4,2,1,4,4,4,4,4,3,4,4,3,2,4,4,4,3,4,4,4,3,4,4,4,2,4,4,3,1,4,3,3,2,2,2,3,2,2,4,1,4,4,4,2,4,3,2,4,2,2,2,2,4,4,4,2,4,2,2,4,4,2,4,1])
rels = []
missing_questions = 0
l_1 = 0
l_2 = 0
l_3 = 0
with open("../eval_results/google/rels_trec2016.txt", "r") as lines:
    for line in lines:
        line = line.rstrip()
        #rel = line.split(" ")[1].strip()
        if "Bank of America s in Alaska" in line:
            print(line)
        """
        q_l = len(line.split(" ")[2].strip().split())
        if "NONE" in line:
            if q_l < 10:
                l_1+=1
            elif q_l > 10 and q_l <= 32:
                l_2+=1
            else:
                l_3+=1
        
        #rel = int(line.split(" ")[1].strip())
        if rel == 4 and isinstance(rel, int):
            print(line)
        rels.append(rel)
        """
"""
metrics = Metrics(rels)
print("avgScore: " , metrics.avg_score())
for i in range(2, 5):
    print("succesAt", i, metrics.success_at(i))
print(l_1, l_2, l_3)

for i in range(2, 5):
    print("precissionAt", i, metrics.precission_at(i))
print("NDCG: " , metrics.ndcg_at())
print("No results: ", metrics.questions_with_no_results())
"""



