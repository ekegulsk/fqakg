from system.test_session import *
from config import Config

class Measure:
    VALUE_SEPARATOR = " "

    def __init__(self, group_name, measure_name, enabled=Config.collect_stats, file_path=None):
        self._group_name = group_name
        self._measure_name = measure_name
        self._enabled = enabled
        self._file_path = file_path
        self._file = None

        if self._enabled and self._file_path is not None:
            self._file = open("{}/{}".format(fqakg_test_session_dir(), file_path), "w", encoding="utf8")


class CountCollector(Measure):
    def __init__(self, group_name, measure_name, enabled=Config.collect_stats, file_path=None):
        super().__init__(group_name, measure_name, enabled, file_path)
        self._count = 0

    def set(self, x):
        self._count = x

    def __add__(self, x):
        self._count = self._count + x
        return self

    def __lt__(self, other):
        return self._count < other

    def __le__(self, other):
        return self._count <= other

    def __eq__(self, other):
        return self._count == other

    def print(self, out_of=None):
        if self._enabled:
            if out_of:
                print("{}.{}: {} ({:0.2f} %)".format(self._group_name, self._measure_name, self._count, self._count*100/out_of))
            else:
                print("{}.{}: {}".format(self._group_name, self._measure_name, self._count))

    def value(self):
        return self._count


class ValuesCollector(Measure):
    def __init__(self, group_name, measure_name, enabled=Config.collect_stats, file_path=None, value_entry_separator=Measure.VALUE_SEPARATOR):
        super().__init__(group_name, measure_name, enabled, file_path)
        self._values = []
        self._value_entry_separator = value_entry_separator

    def add_value(self, value, std_out=False, separator=None):
        if self._enabled:
            self._values.append(value)

            if std_out:
                print(value)

            if self._file:
                self._file.write(str(value))
                if separator is None:
                    self._file.write(self._value_entry_separator)
                else:
                    self._file.write(separator)
                self._file.flush()
