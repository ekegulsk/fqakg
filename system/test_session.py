import sys
import os
import time
import re

KGQA_EVAL_DIR = "eval_results"
test_session_name = ""


# https://stackoverflow.com/questions/616645/how-to-duplicate-sys-stdout-to-a-log-file
class Logger(object):
    def __init__(self, log_file):
        global test_session_name
        self.terminal = sys.stdout
        self.log = open(log_file, "w")

    def write(self, message):
        # credit https://stackoverflow.com/questions/14693701/how-can-i-remove-the-ansi-escape-sequences-from-a-string-in-python
        def escape_ansi(line):
            ansi_escape = re.compile(r'(?:\x1B[@-_]|[\x80-\x9F])[0-?]*[ -/]*[@-~]')
            return ansi_escape.sub('', line)

        self.terminal.write(message)

        message_without_ansi_codes = escape_ansi(message)
        self.log.write(message_without_ansi_codes)
        self.log.flush()


def fqakg_create_test_session(name):
    global test_session_name
    test_session_name = name

    if not os.path.exists(KGQA_EVAL_DIR):
        os.makedirs(KGQA_EVAL_DIR)
    os.mkdir("{}/{}".format(KGQA_EVAL_DIR, test_session_name))
    sys.stdout = Logger("{}/{}/{}".format(KGQA_EVAL_DIR, test_session_name, "fqakg.log"))


def fqakg_test_session_dir():
    global test_session_name
    if test_session_name == "":
        sys.exit("Test Session not set")
    return "{}/{}".format(KGQA_EVAL_DIR, test_session_name)


def fqakg_test_contains_answer(s, answer):
        matched_strings = 0
        if isinstance(s, list):
            for item in s:
                if isinstance(item, dict):
                    for key, value in item.items():
                        matched_strings += len(re.findall(answer, str(value), re.IGNORECASE))
                else:
                    matched_strings += len(re.findall(answer, str(item), re.IGNORECASE))
        else:
            matched_strings = len(re.findall(answer, str(s), re.IGNORECASE))
        return matched_strings


def fqakg_test_answer_rank_from_list(s, answer):
    rank = -1
    for index, item in enumerate(s):
        matched_strings = len(re.findall(answer, str(item), re.IGNORECASE))
        if matched_strings > 0:
            rank = index+1
            break # stop on first answer found
    return rank

cur_timestamp = time.strftime("%Y%m%d-%H%M%S")
fqakg_create_test_session("fqakq_test_{}".format(cur_timestamp))