import sys

sys.path.append(".")
sys.path.append("./modules")
sys.path.append("./metrics")
sys.path.append("./kg-instances")
sys.path.append("./kg-instances/sfsu_diffbot")
sys.path.append("./kg-instances/google_kg_client")

print("Number of arguments: ", len(sys.argv))
print("The arguments are: ", str(sys.argv))

import spacy

nps = spacy.load('en')

from KGQA import *
from instances import *
question = "How tall is Mount McKinley?"
if len(sys.argv) > 1:
    question = sys.argv[1]
question_id = 1
diffbot_instance = Instances.Diffbot(Instances)
google_instance = Instances.Google(Instances)
dkg_key = "xxx" # put your diffbot Knowledge Graph API Key
gkg_key = "xxx" # put your google knowledge graph API Key
dkgqa = KGQA(question, question_id, verbose=True, kg_instance=diffbot_instance, api_key=dkg_key) # using DKG
gkgqa = KGQA(question, question_id, verbose=True, kg_instance=google_instance, api_key=gkg_key) # using GKG
dkgqa_answer = dkgqa.answer()
gkgqa_answer = gkgqa.answer()