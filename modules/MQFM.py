"""
This file implements the Multiquery Formulation Module (MQFM)

Package: kgqa

Author: Jose Ortiz

"""

from nltk import ngrams

class MQFM(object):
    """
        MQFM modulee takes a free-text input and formulates N-grams from it, and the
        correspondent multi-queries using those grams. 
    """

    def __init__(self, qpm):
        """
        Class constructor.
        :param free_text: 
        """

        self._query = qpm.get_sanitazed_sentence()
        self._tokens = qpm.tokens()
        self._original_q = qpm.free_text()

    def original_question(self):
        return self._original_q

    def grams(self):
        """
        Creates N-grams from a free-text input after stop-word removal. 
        e.g: "Who invented the World Wide Web?" -> [invented], [World], [Wide], [Web], [invented, World], 
             [World, Wide], [Wide, Web], [invented, World, Wide], [World, Wide, Web], [invented, World, Wide, Web]
        :return: the N-grams from the input.
        """
        grams = {}
        len_tokens = len(self._tokens)
        for i in range(0, len_tokens + 1):
            n_grams = ngrams(self._tokens, i)
            for gram in n_grams:
                w = ""
                for g in gram:
                    w += (g + " ")
                grams[w.strip()] = {}
        return grams

    def multiquery(self):
        """
        Creates queries from N-grams using all their possible conbinations and permutations.
        e.g "Who invented the World Wide Web?" -> ['"invented World" Wide Web', 'invented "World Wide" Web', 
                                                     'invented World "Wide Web"', '"invented World Wide" Web', 
                                                     'invented "World Wide Web"', '"invented World Wide Web"', 
                                                     'invented World Wide Web']
        :return: a multi-query formulation from N-grams.

        """
        tokens = self._tokens
        queries = []
        for gram in self.grams():
            is_sequence = False
            if len(gram.split()) > 1:
                temporal_query = ""
                for w in tokens:
                    if w in gram and is_sequence is False:
                        temporal_query += "\""
                        is_sequence = True
                    if w not in gram and is_sequence is True:
                        temporal_query = temporal_query.strip()
                        temporal_query += "\" "
                        is_sequence = False
                    temporal_query += w + " "
                if is_sequence is True:
                    temporal_query = temporal_query.strip()
                    temporal_query += "\""
                queries.append(temporal_query.strip())
        # last query with all individual tokens
        queries.append(self._query)
        return queries

    def query(self):
        return self._query
