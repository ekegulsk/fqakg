#
# This test evaluates Live QA 2016 data for Jaccard and cosine similarities.

from test_utils import *

#print("Number of arguments: ", len(sys.argv))
#print("The arguments are: ", str(sys.argv))

kg_instance = "google"

if sys.argv[1] == "-kg":
    kg_instance = sys.argv[2]

questions = []
q_answers = []
answ = ""
rel = 0
question_id = 0
index = 0
num_q_1 = 0
num_q_2 = 0
num_q_3 = 0
MAX_QUESTION = 5000

with open("datasets/LiveQA2016-qrels-ver2.txt", "r", encoding="utf8") as lines:
    for line in lines:
        line = line.rstrip()
        line_parts = line.split()
        q_id = line_parts[0]
        if q_id == question_id:

            tmp_rel = int(line_parts[2])
            if rel < tmp_rel:
                rel = tmp_rel
                answ = get_trec_answ_2016(line_parts)

            continue
        elif "QUESTION" in line_parts:
            num_q = len(get_question(line_parts).split())
            if index > 0:
                q_answers.append(answ)
            if index == MAX_QUESTION:
                    break
            if num_q <=10:
                num_q_1+=1
            elif num_q > 10 and num_q <=32:
                num_q_2 += 1
            elif num_q > 32:
                num_q_3+=1
            questions.append(get_question(line_parts))
            rel = 0
            answ = ""
            question_id = line_parts[1]
            index +=1

# append last answer
q_answers.append(answ)

if kg_instance == "google":
    non_factoid_eval("eval_results/rels_trec2016_googlekg.txt", questions, q_answers, kg_instance, False)
else:
    non_factoid_eval("eval_results/rels_trec2016_diffbotkg.txt", questions, q_answers, kg_instance, False)


print("num_questions_2016", num_q_1, num_q_2, num_q_3)






