#
# This test uses Live QA 2015 data source for testing KGQA system using both Diffbot and Google APIs.
# The results are written to eval_results/diffbot_pres_data.txt

from test_utils import *

#print("Number of arguments: ", len(sys.argv))
#print("The arguments are: ", str(sys.argv))

kg_instance = "google"

if sys.argv[1] == "-kg":
    kg_instance = sys.argv[2]

# Configuration:
MAX_QUESTIONS = 5000  # limit the number of questions read from the Live QA 2015 data source

# Jaccard and cosine similarities experiments
questions = []
q_answers = []
answ = ""
rel = 0
question_id = 0
index = 0
num_q_1 = 0
num_q_2 = 0
num_q_3 = 0

with open("datasets/LiveQA2015-qrels-ver2.txt", "r", encoding="utf8") as lines:
    for line in lines:
        line = line.rstrip()
        line_parts = line.split()
        q_id = line_parts[0]
        if q_id == question_id:
            tmp_rel = int(line_parts[2])
            if rel < tmp_rel:
                rel = tmp_rel
                answ = get_trec_answ(line_parts)

        else:

            if index > 0:
                q_answers.append(answ)

            if index  == MAX_QUESTIONS:
                break

            questions.append(get_question(line_parts))


            num_q = len(get_question(line_parts).split())
            if num_q <=10:
                num_q_1+=1
            elif num_q > 10 and num_q <=32:
                num_q_2 += 1
            elif num_q > 32:
                num_q_3+=1
            rel = 0
            answ = ""
            question_id = q_id
            index +=1
# append last answer
q_answers.append(answ)

# file_path = "eval_results/diffbot_pres_data.txt"
# os.makedirs(os.path.dirname(file_path), exist_ok=True)
# file = open(file_path, "w")
# for q in questions:
#     answ1 = kgqa(q, 0, "dkg", os.environ.get("TOKEN"))
#     answ2 = kgqa(q, 0, "gkg", "AIzaSyC2HvVO8gvxdM2uX6M6PWItK7Y6cuIkVbM")
#     write_sample(file, q, answ1, answ2, 'a')

if kg_instance == "google":
    non_factoid_eval("eval_results/rels_trec2015_googlekg.txt", questions, q_answers, kg_instance, False)
else:
    non_factoid_eval("eval_results/rels_trec2015_diffbotkg.txt", questions, q_answers, kg_instance, False)

print("num_questions_2015", num_q_1, num_q_2, num_q_3)


