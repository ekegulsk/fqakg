import argparse
import json

parser = argparse.ArgumentParser()

parser.add_argument('--in-file', dest='in_file', required=True, help="freebaseqa JSON file to convert")
parser.add_argument('--out-file', dest='out_file', required=True, help="text file with converted questions")

args = parser.parse_args()

def write_out(out_file_handle, q_id, q_type, question, labeled_answer):
    if out_file_handle.tell() > 0:
        out_file_handle.write("\n")

    out_file_handle.write(str(q_id))
    out_file_handle.write("\t")
    out_file_handle.write(q_type)
    out_file_handle.write("\t")
    out_file_handle.write(question)
    out_file_handle.write("\t")
    out_file_handle.write(labeled_answer)


def format_as_re(text):
    def is_wiki_link(text):
        if text.startswith("https://") or text.startswith("http://"):
            return True
        else:
            return False

    result_re = ""

    if is_wiki_link(text):
        wiki_title = text.split('/')
        wiki_title = wiki_title[-1]

        tokens = wiki_title.split('(')
        main_text = tokens[0]
        main_text = " ".join(main_text.split('_')).strip()
        result_re = '\\b' + main_text + '\\b'

        if len(tokens) > 1:
            parenthasis_text = tokens[1].split(')')[0]
            parenthasis_text = " ".join(parenthasis_text.split('_')).strip()
            result_re += "|" + '\\b' + parenthasis_text + '\\b'
    else:
        result_re = '\\b' + text + '\\b'

    return result_re

def convert_freebaseqa(in_file, out_file):
    out_file_handle = open(out_file, "w", encoding="utf8")

    q_id = 0
    q_type = "factoid"

    with open(in_file, "r", encoding="utf8") as json_file:
        data = json.load(json_file)

        for index, item in enumerate(data['Questions']):
            question = item['RawQuestion']
            if item['Parses'] and item['Parses'][0]:
                if item['Parses'][0]['Answers'] and item['Parses'][0]['Answers'][0]:
                    answers = item['Parses'][0]['Answers'][0]['AnswersName']
                    if answers[0]:
                        labeled_answer = format_as_re(answers[0])
                        write_out(out_file_handle, q_id, q_type, question, labeled_answer)
                        q_id += 1
    out_file_handle.close()

if __name__ == "__main__":
    convert_freebaseqa(args.in_file, args.out_file)