#
# This test uses Live QA 2015 data source for testing KGQA system using both Diffbot and Google APIs.
# The results are written to eval_results/diffbot_pres_data.txt

import argparse
import sys
sys.path.append("../system")
from config import Config

def check_kg(value):
    if value == 'dkg' or value == 'gkg':
         return value
    else:
        raise argparse.ArgumentTypeError("%s is an invalid kg_instance. Valid values are: 'gkg' and 'dkg'" % value)

parser = argparse.ArgumentParser()
parser.add_argument('--kg', dest='kg_instance', required=True, type=check_kg, help="Specify kg_instance, choices are 'gkg' for Google KG or 'dkg' for Diffbot KG")
parser.add_argument('--kg-api-key', dest='kg_api_key', required=True, help="KG API Key")
parser.add_argument('--factoid-pipeline', dest='factoid_pipeline', action='store_true', help="Use factoid pipeline in KGQA2")
parser.add_argument('--enable-stats', dest='enable_stats', action='store_true', help="Enable stats collection")

args = parser.parse_args()
Config.collect_stats = args.enable_stats

from test_utils import *
factoid_eval(in_file="datasets/curated-test-factoid-cleaned.txt",
             kginstance=args.kg_instance,
             kg_api_key=args.kg_api_key,
             factoid_pipeline=args.factoid_pipeline)