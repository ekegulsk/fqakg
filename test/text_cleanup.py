import argparse

parser = argparse.ArgumentParser()

parser.add_argument('--in-file', dest='in_file', required=True, help="text file to clean")
parser.add_argument('--out-file', dest='out_file', required=True, help="text file with cleaned text")
parser.add_argument('--clean-re', dest='clean_re', required=True, help="clean regular expression at provided tab position")

args = parser.parse_args()

def fact_line_parts(line):
    parts = line.split("\t")
    q_id = parts[0]
    q_type = parts[1]
    question = parts[2]
    answer = parts[3]

    return q_id.strip(), q_type.strip(), question.strip(), answer.strip()


def clean_re(reg_expr):
    cleaned_re = reg_expr
    cleaned_alternate_exprs = []
    alternate_exprs = []

    in_group = False
    cur_expr = ""
    for c in reg_expr:
        if c == '(':
            in_group = True
            cur_expr += c
            continue
        if in_group and c == ')':
            in_group = False
            cur_expr += c
            continue
        if c == '|' and not in_group:
            alternate_exprs.append(cur_expr)
            cur_expr = ""
            continue
        cur_expr += c

    if cur_expr != "":
        alternate_exprs.append(cur_expr)

    for re in alternate_exprs:
        if re[:2] != '\\b':
            #print("fixing {}".format(re))
            re = '\\b' + re

        if re[-2:] != '\\b':
            #print("fixing {}".format(re))
            re = re + '\\b'

        cleaned_alternate_exprs.append(re)

    cleaned_re = '|'.join(cleaned_alternate_exprs)
    return cleaned_re

def write_out(out_file_handle, q_id, q_type, question, labeled_answer):
    if out_file_handle.tell() > 0:
        out_file_handle.write("\n")

    out_file_handle.write(q_id)
    out_file_handle.write("\t")
    out_file_handle.write(q_type)
    out_file_handle.write("\t")
    out_file_handle.write(question)
    out_file_handle.write("\t")
    out_file_handle.write(labeled_answer)


def clean_text(in_file, out_file, re_tab_position):
    out_file_handle = open(out_file, "w", encoding="utf8")

    with open(in_file, "r", encoding="utf8") as lines:
        for line in lines:
            line = line.rstrip()
            data = fact_line_parts(line)
            q_id = data[0]
            q_type = data[1]
            question = data[2]
            labeled_answer = data[3]
            labeled_answer_cleaned = clean_re(labeled_answer)
            write_out(out_file_handle, q_id, q_type, question, labeled_answer_cleaned)

    out_file_handle.close()

if __name__ == "__main__":
    clean_text(args.in_file, args.out_file, args.clean_re)