#
# Utilities

import sys

sys.path.append("..")
sys.path.append("../modules")
sys.path.append("../metrics")
sys.path.append("../kg-instances")
sys.path.append("../kg-instances/sfsu_diffbot")
sys.path.append("../kg-instances/google_kg_client")

from KGQA import *
import spacy
import re
import math
from collections import Counter
import random
from system.test_session import *
from colorama import init
from colorama import Fore, Back, Style
from termcolor import colored
# use Colorama to make Termcolor work on Windows too
init()

nps = spacy.load('en')
WORD = re.compile(r'\w+')

NUMBER_TOP_ANSWERS_TO_EVALUATE = 3
RECALL_LIST_SIZE = 200

def kgqa(question, index, instance, key_api, single_answer):
    """
    try:
        kgqa = KGQA(question, index, verbose=True, kg_instance="dkg", api_key=os.environ.get("TOKEN"))
        return kgqa.answer()
    except Exception as e:
        print(repr(e))
    """

    kgqa = KGQA(question, index, verbose=False, kg_instance=instance, api_key=key_api, generate_single_answer=single_answer)
    return kgqa.answer()

def get_cosine(vec1, vec2):
     intersection = set(vec1.keys()) & set(vec2.keys())
     numerator = sum([vec1[x] * vec2[x] for x in intersection])

     sum1 = sum([vec1[x]**2 for x in vec1.keys()])
     sum2 = sum([vec2[x]**2 for x in vec2.keys()])
     denominator = math.sqrt(sum1) * math.sqrt(sum2)

     if not denominator:
        return 0.0
     else:
        return float(numerator) / denominator

def text_to_vector(text):
     words = WORD.findall(text)
     return Counter(words)



def fact_line_parts(line):
    parts = line.split("\t")
    q_id = parts[0]
    question = parts[2]
    answer = parts[3]

    return q_id.strip(), question.strip(), answer.strip()



def get_trec_answ(line_parts):
    answ = ""
    for i in range(2, len(line_parts)):
        if "http" not in line_parts[i]:
            answ += " " + line_parts[i]
    return answ

def get_trec_answ_2016(line_parts):
    answ = ""
    for i in range(2, len(line_parts)):
        answ += " " + line_parts[i]
    return answ


def DistJaccard(str1, str2):
    str1 = set(str1.split())
    str2 = set(str2.split())
    return (float(len(str1 & str2)) / len(str1 | str2)) + 0.1

def get_question(line_parts):
    question_parts = []
    index = 0
    for part in line_parts:
        if index > 1:
            question_parts.append(part)
        index+=1
    return " ".join(question_parts)

def cos_sim(str1, str2):
    vector1 = text_to_vector(str1)
    vector2 = text_to_vector(str2)
    return get_cosine(vector1, vector2)

def write(file, question_id, question, answer, cos, jac, matched_answer=None):
    data = question_id + ", " + "  " + question + ",  " + answer + ", " + str(cos) + ", " + str(jac)
    if matched_answer is not None:
        data = data + ", matched_answer=" + str(matched_answer)

    file.write(data)
    file.write("\n")

def contains_answer(s, answer):
    matched_strings = 0
    if isinstance(s, list):
        for item in s:
            matched_strings += len(re.findall(answer, item, re.IGNORECASE))
    else:
        matched_strings = len(re.findall(answer, s, re.IGNORECASE))
    return matched_strings

def print_alighned_pair(str1, str2):
    LINE_LENGTH = 28
    print(str1, end="")
    print(' ' * (LINE_LENGTH-len(str1)), end="")
    print(str2)

def print_factoid_stats(kgqa, index, question, labeled_answer, kgqa_answ,factoid_pipeline, matched_answer, matched_rank,
                        total_correct_answers, no_answers, duration, total_duration, correct_ranked_answers, MRR):
    print_alighned_pair("QUESTION", "{}".format(question))
    print_alighned_pair("LABELED ANSWER", "{}".format(labeled_answer))
    print_alighned_pair("PIPELINE DETAILS",
                        "{}".format("Factoid pipeline" if factoid_pipeline else "Non-factoid pipeline"))
    if isinstance(kgqa_answ, list):
        print_alighned_pair("KGQA ANSWERS",
                            "{} correct answers".format(matched_answer))
        correct_printed = False

        for i, item in enumerate(kgqa_answ):
            if i < NUMBER_TOP_ANSWERS_TO_EVALUATE:
                if i == matched_rank-1:
                    print(Fore.LIGHTCYAN_EX, end="")
                    print_alighned_pair("    correct answer[{}]:".format(i+1), "{}".format(item))
                    print(Style.RESET_ALL, end="")
                    correct_printed = True
                else:
                    print_alighned_pair("    answer[{}]:".format(i+1), "{}".format(item))

            if not correct_printed and i == matched_rank-1:
                print(Fore.LIGHTCYAN_EX, end="")
                print_alighned_pair("    correct answer[{}]:".format(i+1), "{}".format(item))
                print(Style.RESET_ALL, end="")
    else:
        print_alighned_pair("KGQA SINGLE ANSWER", "{}".format(kgqa_answ))

    print_alighned_pair("MATCHING ANSWER FOUND?", "{}".format("YES" if matched_answer else "NO"))
    total_accuracy = 0
    num_recall_answers = RECALL_LIST_SIZE
    recall = 0

    correct_ranked_answers_sorted = dict(sorted(correct_ranked_answers.items()))
    for k,v in correct_ranked_answers_sorted.items():
        if k <= NUMBER_TOP_ANSWERS_TO_EVALUATE:
            print_alighned_pair("Accuracy@{}".format(k), "{:.2%}".format(v/index))
            total_accuracy += v / index

        if k <= RECALL_LIST_SIZE:
            recall += v/index
            num_recall_answers -= 1
        else:
            break # we are done

    print_alighned_pair("Accuracy@(1+2+3)", "{:.2%}".format(total_accuracy))
    print_alighned_pair("Recall@{}".format(RECALL_LIST_SIZE), "{:.2%}".format(recall))
    print_alighned_pair("Mean Reciprocal Rank (MRR)", MRR)
    print_alighned_pair("STATS:",
                            "Total questions: {}, correct answers: {} ({:.2%}), no answers: {}, duration: {}, ave time: {}".format(
                                index, total_correct_answers, total_correct_answers/index, no_answers, duration, total_duration / index))
    print("")

def factoid_eval(in_file, kginstance, kg_api_key, factoid_pipeline):
    index = 0
    total_correct_answers = 0
    no_answers = 0
    total_duration = 0
    correct_ranked_answers = {}
    one_over_rank_total_sum = 0
    cur_MRR = 0

    factoid_correct_ans = open("{}/factoid_correct_ans.txt".format(fqakg_test_session_dir()), "w", encoding="utf8")
    factoid_wrong_ans = open("{}/factoid_wrong_ans.txt".format(fqakg_test_session_dir()), "w", encoding="utf8")

    with open(in_file, "r", encoding="utf8") as lines:
        for line in lines:
            index += 1

            line = line.rstrip()
            data = fact_line_parts(line)
            q_id = data[0]
            question = data[1]
            labeled_answer = data[2]

            before = datetime.datetime.now()
            print(Style.BRIGHT, end="")
            print("Evaluating question: {}".format(question))
            print(Style.RESET_ALL, end="")
            kgqa = KGQA(question=question,
                        labeled_answer=labeled_answer,
                        question_id=index,
                        verbose=False,
                        kg_instance=kginstance,
                        api_key=kg_api_key,
                        factoid_pipeline=factoid_pipeline)
            kgqa_answ = kgqa.answer()
            after = datetime.datetime.now()
            timespan = after - before
            duration = int(timespan.total_seconds() * 1000)

            rank = -1
            if kgqa_answ == "":
                no_answers += 1
                matched_answer = False
                cur_MRR = one_over_rank_total_sum / index
            else:
                if len(kgqa_answ) > 200:
                    print("kgqa_answ size is bigger than 200: {}".format(len(kgqa_answ)))
                else:
                    print("kgqa_answ size is {}".format(len(kgqa_answ)))

                rank = fqakg_test_answer_rank_from_list(kgqa_answ, labeled_answer)
                matched_answer = fqakg_test_contains_answer(kgqa_answ, labeled_answer)

                if rank != -1:
                    if rank in correct_ranked_answers:
                        correct_ranked_answers[rank] += 1
                    else:
                        correct_ranked_answers[rank] = 1

                    # calculate MRR
                    one_over_rank = 1/rank
                    one_over_rank_total_sum = one_over_rank_total_sum + one_over_rank
                    cur_MRR = one_over_rank_total_sum/index

                    total_correct_answers += 1
                else:
                    cur_MRR = one_over_rank_total_sum / index

            total_duration += duration
            print_factoid_stats(kgqa=kgqa, index=index, question=question, labeled_answer=labeled_answer, kgqa_answ=kgqa_answ,
                                factoid_pipeline=factoid_pipeline, matched_answer=matched_answer, matched_rank=rank,
                                total_correct_answers=total_correct_answers, no_answers=no_answers, duration=duration,
                                total_duration=total_duration, correct_ranked_answers=correct_ranked_answers, MRR=cur_MRR)

            # write to files correct and wrong answers
            if 1 <= rank <= 3:
                factoid_correct_ans.write(line)
                factoid_correct_ans.write("\n")
                factoid_correct_ans.flush()
            else:
                factoid_wrong_ans.write(line)
                factoid_wrong_ans.write("\n")
                factoid_wrong_ans.flush()

    print("")
    print("--------------------------- KGQA2 Results ---------------------------")
    print_alighned_pair("KG", kginstance)
    print_alighned_pair("Test set", in_file)
    print_alighned_pair("Total questions", index)
    print_alighned_pair("Total correct answers", total_correct_answers)

    total_accuracy = 0
    num_recall_answers = RECALL_LIST_SIZE
    recall = 0

    correct_ranked_answers_sorted = dict(sorted(correct_ranked_answers.items()))
    for k,v in correct_ranked_answers_sorted.items():
        if k <= NUMBER_TOP_ANSWERS_TO_EVALUATE:
            print_alighned_pair("Accuracy@{}".format(k), "{:.2%}".format(v/index))
            total_accuracy += v/index

        if k <= RECALL_LIST_SIZE:
            recall += v/index
            num_recall_answers -= 1
        else:
            break # we are done

    print_alighned_pair("Accuracy@(1+2+3)", "{:.2%}".format(total_accuracy))
    print_alighned_pair("Recall@{}".format(RECALL_LIST_SIZE), "{:.2%}".format(recall))
    print_alighned_pair("Mean Reciprocal Rank (MRR)", cur_MRR)
    print_alighned_pair("Average answer time (ms)", total_duration / index)

def non_factoid_eval(out_file, questions, answers, kginstance, random_eval=True):
    if random_eval:
        evaluated = random.sample(range(1, 1087), 100)
    else:
        evaluated = {}
    index = 0
    j_t_1 = 0.00
    j_t_2 = 0.00
    j_t_3 = 0.00
    c_t_1 = 0.00
    c_t_2 = 0.00
    c_t_3 = 0.00
    l_q_1 = 0
    l_q_2 = 0
    l_q_3 = 0
    l_uq_1 = 0
    l_uq_2 = 0
    l_uq_3 = 0
    num_no_answers = 0

    os.makedirs(os.path.dirname(out_file), exist_ok=True)
    file = open(out_file, "w",encoding="utf8")

    for question in questions:
        if not random_eval or (index in evaluated):
            if kginstance == "google":
                answ = kgqa(question, index, "gkg", "AIzaSyC2HvVO8gvxdM2uX6M6PWItK7Y6cuIkVbM")
            else:
                answ = kgqa(question, index, "dkg", os.environ.get("TOKEN"))
                time.sleep(10)

            l_q = len(question.split())
            if answ == "No Answer":
                num_no_answers += 1
                if l_q > 0 and l_q <= 10:
                    l_uq_1 += 1
                elif l_q > 10 and l_q <= 32:
                    l_uq_2 += 1
                elif l_q > 32:
                    l_uq_3 += 1
                continue
            trec_answ = answers[index]
            cos = cos_sim(answ, trec_answ)
            jac = DistJaccard(answ, trec_answ)
            if l_q > 0 and l_q <=10:
                l_q_1 +=1
                j_t_1 += jac
                c_t_1 += cos
            elif l_q >10 and l_q <=32:
                l_q_2 += 1
                j_t_2 += jac
                c_t_2 += cos
            elif l_q > 32 :
                l_q_3 += 1
                j_t_3 += jac
                c_t_3 += cos
            write(file, str(index), question, answ, cos, jac)
        index+=1
        if l_q_1 == 0:
            c1 = 0
        else:
            c1 = c_t_1/l_q_1
    print(safe_div(c_t_1,l_q_1), safe_div(j_t_1,l_q_1), safe_div(c_t_2,l_q_2), safe_div(j_t_2,l_q_2), safe_div(c_t_3,l_q_3), safe_div(j_t_3,l_q_3))
    file.write("Cumulative Cosine and Jacard Similarity\n")
    data = str(safe_div(c_t_1,l_q_1)) + ", " + str(safe_div(j_t_1,l_q_1)) + ", " + str(safe_div(c_t_2,l_q_2)) + ", " + str(safe_div(j_t_2,l_q_2)) + ", " + str( safe_div(c_t_3,l_q_3)) + ", " + str(safe_div(j_t_3,l_q_3))
    file.write(data)
    file.write("\n")
    file.write("Unanswered questions: " + str(num_no_answers) + "(" + str(l_uq_1) + ", " + str(l_uq_2) + ", " + str(l_uq_3) + ")")
    file.write("\n")

def safe_div(x,y):
    if y == 0:
        return 0
    return x / y

def write_sample(file, question, answer1, answer2, w_or_a):
    q_data = "Question: " +  question
    file.write(q_data)
    dkgqa = "DKGQA: " + answer1
    gkgqa = "GKGQA: " + answer2
    file.write("\n")
    file.write(dkgqa)
    file.write("\n")
    file.write(gkgqa)
    file.write("\n")