

from sfsu_diffbot import *
from modules.QPM import *
from modules.MQFM import *
from modules.FMQFM import *
from modules.DSOEM import *
from modules.AESM import *
from modules.FAESM import *

class KGQA(object):

    def __init__(self, question, labeled_answer, question_id, verbose, kg_instance="dkg", api_key=None, factoid_pipeline=False):
        self._question = question
        self._question_id = question_id
        self._verbose = verbose
        self._kg_instance = kg_instance
        self._api_key = api_key
        self._factoid_pipeline = factoid_pipeline
        self._aesm = None
        self._kgoem = None
        self._mqfm = None
        self._qpm = None
        self._labeled_answer=labeled_answer

    def answer(self):
        self._qpm = QPM(self._question, self._labeled_answer)
        if self._factoid_pipeline:
            self._mqfm = FMQFM(self._qpm)
            self._kgoem = DSOEM(self._mqfm, self._kg_instance, self._api_key, self._qpm)
            # use FAESM module for factoid question selection
            self._aesm = FAESM(dsoem=self._kgoem)
        else:
            self._mqfm = MQFM(self._qpm)
            self._kgoem = KGOEM(self._mqfm, self._kg_instance, self._api_key, self._qpm)
            self._aesm = AESM(kgoem=self._kgoem, apply_kgqa_score=True)

        if self._aesm:
            answ = self._aesm.top_answers()
        else:
            answ = ""

        if self._verbose:
            print("question_id: ", self._question_id)
            print("question: ", self._question)
            if isinstance(answ, list):
                index = 0
                for item in answ:
                    print("KGQA answer candidate[{}]: {}".format(index, item))
                    index += 1
            else:
                print("KGQA single answer: {}".format(answ))

        return answ

    def kg_objects(self):
        if self._kgoem:
            return self._kgoem.get_data_objects()
        else:
            return []
