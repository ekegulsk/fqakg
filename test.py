#
# This file performs custom evaluatons experiments of KGQA system, as well as, testing and debugging
#

from KGQA import *
from instances import *
import spacy
import re, math
from collections import Counter
import random
import os
from pprint import pprint

nps = spacy.load('en')
WORD = re.compile(r'\w+')

def kgqa(question, index, instance, key_api):
    """
    try:
        kgqa = KGQA(question, index, verbose=True, kg_instance="dkg", api_key=os.environ.get("TOKEN"))
        return kgqa.answer()
    except Exception as e:
        print(repr(e))
    """

    kgqa = KGQA(question, index, verbose=True, kg_instance=instance, api_key=key_api)
    return kgqa.answer()



def get_cosine(vec1, vec2):
     intersection = set(vec1.keys()) & set(vec2.keys())
     numerator = sum([vec1[x] * vec2[x] for x in intersection])

     sum1 = sum([vec1[x]**2 for x in vec1.keys()])
     sum2 = sum([vec2[x]**2 for x in vec2.keys()])
     denominator = math.sqrt(sum1) * math.sqrt(sum2)

     if not denominator:
        return 0.0
     else:
        return float(numerator) / denominator

def text_to_vector(text):
     words = WORD.findall(text)
     return Counter(words)



def fact_line_parts(line):
    parts = line.split()
    question_tokens = []
    is_question_token = False
    q_id = parts[0]
    for part in parts:
        if is_question_token:
            question_tokens.append(part)
            if "?" in part:
                is_question_token = False
        if part == "factoid":
            is_question_token = True
    question = " ".join(question_tokens)
    return q_id.strip(), question.strip()



def get_trec_answ(line_parts):
    answ = ""
    for i in range(2, len(line_parts)):
        if "http" not in line_parts[i]:
            answ += " " + line_parts[i]
    return answ

def get_trec_answ_2016(line_parts):
    answ = ""
    for i in range(2, len(line_parts)):
        answ += " " + line_parts[i]
    return answ


def DistJaccard(str1, str2):
    str1 = set(str1.split())
    str2 = set(str2.split())
    return (float(len(str1 & str2)) / len(str1 | str2)) + 0.1

def get_question(line_parts):
    question_parts = []
    index = 0
    for part in line_parts:
        if index > 1:
            question_parts.append(part)
        index+=1
    return " ".join(question_parts)

def cos_sim(str1, str2):
    vector1 = text_to_vector(str1)
    vector2 = text_to_vector(str2)
    return get_cosine(vector1, vector2)

def write(file_path, question_id, question, answer, w_or_a):
    file = open(file_path, w_or_a)
    data = question_id + ", " + " " + ", " +  question + ",  " + answer + " "
    file.write(data)
    file.write("\n")

def factoid_eval(in_file, out_file, random_eval=True):
    evaluated = random.sample(range(1, 867), 100)
    q_index = 1
    index = 1
    with open(in_file, "r") as lines:
        for line in lines:
            if q_index in evaluated:
                line = line.rstrip()
                data = fact_line_parts(line)
                q_id = data[0]
                question = data[1]
                answ = kgqa(question, index,"gkg", "xxx")
                write(out_file, q_id, question, answ, "a")
                index+=1
            q_index+=1

def non_factoid_eval(out_file, questions, answers, random_eval=True):
    evaluated = random.sample(range(1, 1087), 100)
    index = 0
    j_t_1 = 0.00
    j_t_2 = 0.00
    j_t_3 = 0.00
    c_t_1 = 0.00
    c_t_2 = 0.00
    c_t_3 = 0.00
    l_q_1 = 0
    l_q_2 = 0
    l_q_3 = 0
    for question in questions:
        if index in evaluated:
            answ = kgqa(question, index, "gkg", "xxx")
            l_q = len(question.split())
            trec_answ = answers[index]
            cos = cos_sim(answ, trec_answ)
            jac = DistJaccard(answ, trec_answ)
            if l_q > 0 and l_q <=10:
                l_q_1 +=1
                j_t_1 += jac
                c_t_1 += cos
            elif l_q >10 and l_q <=32:
                l_q_2 += 1
                j_t_2 += jac
                c_t_2 += cos
            elif l_q > 32 :
                l_q_3 += 1
                j_t_3 += jac
                c_t_3 += cos
            #write(out_file, str(index), question, answ, "a")
        index+=1
        if l_q_1 == 0:
            c1 = 0
        else:
            c1 = c_t_1/l_q_1
    print(safe_div(c_t_1,l_q_1), safe_div(j_t_1,l_q_1), safe_div(c_t_2,l_q_2), safe_div(j_t_2,l_q_2), safe_div(c_t_3,l_q_3), safe_div(j_t_3,l_q_3))

def safe_div(x,y):
    if y == 0:
        return 0
    return x / y

def write_sample(file_path, question, answer1, answer2, w_or_a):
    file = open(file_path, w_or_a)
    q_data = "Question: " +  question
    file.write(q_data)
    dkgqa = "DKGQA: " + answer1
    gkgqa = "GKGQA: " + answer2
    file.write("\n")
    file.write(dkgqa)
    file.write("\n")
    file.write(gkgqa)
    file.write("\n")

qns = ["How tall is Mount McKinley?"]
for q in qns:
    kgqa(q, 0, "dkg", os.environ.get("TOKEN"))


"""
#answ = kgqa("invented the world wide web?", 1, "gkg", "xxx")
#pprint(answ)
index = 0
with open("datasets/diffbot-presentation-data.txt", "r") as lines:
    for line in lines:
        line = line.rstrip()
        if "QUESTION:" in line:
            index+=1
            line = line.replace("QUESTION:", "")
            answ2 = kgqa(line, 0, "dkg", os.environ.get("TOKEN"))

print(index)


qns = ["What causes disasters in the world?", "I need to know if there are Bank of America s in Alaska? Are there any Bank of Americas in Alaska?",
             "Why have south African reformers not divided white-owned farmland among black farmers?", "Why were the farmers hit hardest by the crash of the stock market? History Arts & Humanities",
       "Persepolis graphic novel? To anyone who had read the graphic novel written by mary jane satrapi can you please help answers my questions thanks. /n/nIn the graphic novel of persepolis what way does it portray the world with a liberating strangeness?and does it break any literacy rules if any what are they?and lastly explain how,plot,characters,setting,or points of views can be developed or crafted differently than in any other non graphical fictional works? Books & Authors Arts & Humanities",
       "What city hosted the 1936 Summer Olympics?", "How many terms was Dwight D. Eisenhower president?", "When was the Hellenistic Age?", "What is the deepest lake in America?", "What is a female rabbit called?",  "what date was the Declaration of Independence signed on?", "When was Ronald Reagan shot?"]

# Jaccard and cosine similarities experiments
questions = []
q_answers = []
answ = ""
rel = 0
question_id = 0
index = 0
dataset=2016
num_q_1 = 0
num_q_2 = 0
num_q_3 = 0

with open("datasets/LiveQA2015-qrels-ver2.txt", "r") as lines:
    for line in lines:
        line = line.rstrip()
        line_parts = line.split()
        q_id = line_parts[0]
        if q_id == question_id:
            tmp_rel = int(line_parts[2])
            if rel < tmp_rel:
                rel = tmp_rel
                answ = get_trec_answ(line_parts)

        else:

            if index > 0:
                q_answers.append(answ)


            questions.append(get_question(line_parts))


            num_q = len(get_question(line_parts).split())
            if num_q <=10:
                num_q_1+=1
            elif num_q > 10 and num_q <=32:
                num_q_2 += 1
            elif num_q > 32:
                num_q_3+=1
            rel = 0
            answ = ""
            question_id = q_id
            index +=1
fl = "eval_results/diffbot_pres_data.txt"
for q in questions:
    answ1 = kgqa(q, 0, "dkg", os.environ.get("TOKEN"))
    answ2 = kgqa(q, 0, "gkg", "xxx")
    write_sample(fl, q, answ1, answ2, 'a')

non_factoid_eval("eval_results/google/rels_trec2015.txt", questions, q_answers)
print("num_questions_2015", num_q_1, num_q_2, num_q_3)


num_q_1 = 0
num_q_2 = 0
num_q_3 = 0
with open("datasets/LiveQA2016-qrels-ver2.txt", "r") as lines:
    for line in lines:
        line = line.rstrip()
        line_parts = line.split()
        q_id = line_parts[0]
        if q_id == question_id:

            tmp_rel = int(line_parts[2])
            if rel < tmp_rel:
                rel = tmp_rel
                answ = get_trec_answ_2016(line_parts)

            continue
        elif "QUESTION" in line_parts:
            num_q = len(get_question(line_parts).split())
            if index > 0:
                q_answers.append(answ)
            if num_q <=10:
                num_q_1+=1
            elif num_q > 10 and num_q <=32:
                num_q_2 += 1
            elif num_q > 32:
                num_q_3+=1
            questions.append(get_question(line_parts))
            rel = 0
            answ = ""
            question_id = line_parts[1]
            index +=1
non_factoid_eval("eval_results/google/rels_trec2016.txt", questions, q_answers)
print("num_questions_2016", num_q_1, num_q_2, num_q_3)

"""







