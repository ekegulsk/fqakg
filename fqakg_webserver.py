#!/usr/bin/env python
import web
import xml.etree.ElementTree as ET
import sys
import spacy

sys.path.append(".")
sys.path.append("./system")
sys.path.append("./modules")
sys.path.append("./metrics")
sys.path.append("./kg-instances")
sys.path.append("./kg-instances/sfsu_diffbot")
sys.path.append("./kg-instances/google_kg_client")

nps = spacy.load('en')

from KGQA import *
from instances import *
question = "How tall is Mount McKinley?"
question_id = 1
diffbot_instance = Instances.Diffbot(Instances)
google_instance = Instances.Google(Instances)
dkg_key = "xxx" # put your diffbot Knowledge Graph API Key
gkg_key = "xxx" # put your google knowledge graph API Key

# tree = ET.parse('user_data.xml')
# root = tree.getroot()

urls = (
    '/users', 'list_users',
    '/users/(.+)', 'get_user',
    '/fqakg/(.+)', 'fqakg'
)

app = web.application(urls, globals())

class list_users:
    def GET(self):
        output = 'users:[';
        for child in root:
            print('child', child.tag, child.attrib)
            output += str(child.attrib) + ','
            output += ']';

        web.header('Access-Control-Allow-Origin', '*')
        web.header('Access-Control-Allow-Credentials', 'true')

        dkgqa = KGQA(question, question_id, verbose=True, kg_instance=diffbot_instance, api_key=dkg_key)  # using DKG
        #gkgqa = KGQA(question, question_id, verbose=True, kg_instance=google_instance, api_key=gkg_key)  # using GKG
        # dkgqa_answer = dkgqa.answer()
        gkgqa_answer = dkgqa.answer()
        return gkgqa_answer

class get_user:
    def GET(self, user):
        web.header('Access-Control-Allow-Origin', '*')
        web.header('Access-Control-Allow-Credentials', 'true')
        print('user=' + user)

        for child in root:
            if child.attrib['id'] == user:
                return str(child.attrib)

class fqakg:
    def GET(self, user_query):
        os.system('clear')
        print('FQAKG Web Service: question = ' + user_query)
        print("")

        web.header('Access-Control-Allow-Origin', '*')
        web.header('Access-Control-Allow-Credentials', 'true')

        question = user_query
        dkgqa = KGQA(question=question,
                     question_id=1,
                     labeled_answer=None,
                     verbose=False,
                     kg_instance=diffbot_instance,
                     api_key=dkg_key,
                     factoid_pipeline=True)
        json_result = json.dumps(dkgqa.answer()[0:3])
        return json_result

if __name__ == "__main__":
    app.run()

