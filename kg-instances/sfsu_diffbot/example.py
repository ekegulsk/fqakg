"""
File:         example.py
Package:      SFSU-Diffbot-API
Author:       Jose Ortiz Costa <jortizco@mail.sfsu.edu>
Date:         09-29-2017
Modified:     09-29-2017
Description:  This file contains examples about how to use the SFSU-Diffbot-API

IMPORTANT:    A valid Diffbot token is needed to run the API.
              A Diffbot token may be invoked from envoronment variables or any other encription method
              like in this example where the token is called from the os.environment 

USAGE:        run python example.py
"""
import os

from sfsu_diffbot.sfsu_diffbot_client import Diffbot

token = os.environ.get("TOKEN")  # diffbot token set previosly in an environment variable
client = Diffbot(token).client()
client.num_results = 2  # optional. Otherwise, default is set to 5 results per response

"""
Example Search Api
"""
query = "(Obama OR Clinton) AND president"
# parameters are optional and will be appended to the query.
data = {'confidence': '0.7', 'type': 'article'}
response = client.search(query=query, param=data)  # response from search api
# check docs for a complete list of object properties
objects = response.objects()
print("Hits: ", response.hits())
for object in objects:
    print("Object details..............................................")
    # some example properties.
    print("type: " , object.type())
    print("details: ", object.title(), object.date(), object.author(), object.url(), object.siteName(), object.humanLanguage())
    print("Text......")
    print(object.text())
    print("sentiment score: ", object.sentiment())


# single object creation
try:
    object = response.object(0)  # highest scored object
except:
    print("Object not found")

# images from objects
try:
    images = object.images()  # all images from a object
    for image in images:
        print(image.title(), image.url())
except:
    print("Images not found")

# videos from objects
try:
    videos = object.videos()
    for video in videos:
        print(video.url(), video.duration())
except:
    print("Videos not found")
"""
Example with the Discussion Extractor API


url = "https://news.ycombinator.com/item?id=5608988"
response = client.discussion(url=url)  # response from search api
discussions = response.objects()
for discus in discussions:
   print("Confidence: ", discus.confidence())
   posts = discus.posts()
   for post in posts:
       print("************ Posts *************")
       print(post.text())
"""








